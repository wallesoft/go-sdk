module gitee.com/wallesoft/go-sdk

go 1.14

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.407
	github.com/gogf/gf v1.13.3
)
