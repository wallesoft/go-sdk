//diyi sms (http://www.1xinxi.cn/) Admin (http://sms.1xinxi.cn/)
package sms

import (
	"bytes"
	"errors"
	"fmt"
	"text/template"

	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

// DriverDiyi is the driver for sms.
type DriverDiyi struct {
	*Core
	Config *ConfigDiyi
	node   *ConfigNode
}

// ConfigDiyi diyi config
type ConfigDiyi struct {
	Name string //用户名
	Pass string //密码
	Sign string //签名
}

const (
	DIYI_SMS_SERVICE_URL = "http://sms.1xinxi.cn/asmx/smsservice.aspx?"
)

// New create and return a sms object
func (d *DriverDiyi) New(core *Core, node *ConfigNode) (SMS, error) {
	if node.Config == nil {
		// glog.Errorf("node Config is nil")
		return nil, errors.New("diyi node Config is nil")
	}
	var config *ConfigDiyi
	if err := gconv.Struct(node.Config.Map(), &config); err != nil {
		return nil, err
	}
	driver := &DriverDiyi{
		Core: core,
		node: node,
		// Data: node.Data,
		Config: config,
	}
	err := core.LoadTemplateFileToData(node) ///////
	if err != nil {
		return nil, err
	}
	return driver, nil
}

// ParseTemplate parese sms template to content
func (d *DriverDiyi) parseTemplate(name string, params map[string]string) (string, error) {
	if d.node.Data.Contains(name) {
		buf := new(bytes.Buffer)
		var tmpl = template.Must(template.New("").Parse(d.node.Data.Get(name)))
		tmpl.Execute(buf, params)
		return buf.String(), nil
	} else {
		d.logger.Error(fmt.Sprintf("diyi sms template '%s' is not exists", name))
	}
	return "", errors.New(fmt.Sprintf("diyi sms template '%s' is not exists", name))
}

// Send send content to the given mobile
func (d *DriverDiyi) Send(mobile string, template string, params map[string]string) error {
	content, err := d.parseTemplate(template, params)
	if err != nil {
		return err
	}
	if err := d.excute(mobile, content); err != nil {
		d.logger.Error(err)
	}
	return nil
}

// Excute
func (d *DriverDiyi) excute(mobile string, content string) error {
	params := map[string]string{
		"name":    d.Config.Name, // 账号
		"pwd":     d.Config.Pass, // 接口密码
		"mobile":  mobile,        // 发送手机号
		"content": content,       // 发送内容
		"stime":   "",            // 可选 发送时间，不填当前时间发送
		"sign":    d.Config.Sign, // 签名
		"type":    "pt",          // 固定值 pt
		"extno":   "",            // 扩展字段
	}
	query := ghttp.BuildParams(params)

	// client
	client := ghttp.NewClient()
	client.SetHeader("Content-Type", "text/html")
	client.SetHeader("charset", "UTF-8")

	if r, err := client.Get(DIYI_SMS_SERVICE_URL + query); err != nil {
		r.Close()
		return errors.New(fmt.Sprintf("Diyi Excute client request error: '%v'", err))
	} else {
		defer r.Close()
		s := r.ReadAllString()
		if len(s) > 0 && fmt.Sprintf(s[0:1]) == "0" {
			return nil
		}
		return errors.New(fmt.Sprintf("Diyi excute request Status: %s", s))
	}
	// return nil
}

// init diyi driver
func (d *DriverDiyi) init() {

}
