package sms

import (
	"github.com/gogf/gf/container/gmap"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/os/gres"
	"github.com/gogf/gf/util/gconv"
)

// Send
func (c *Core) Send(mobile string, template string, params map[string]string) error {
	if err := c.SMS.Send(mobile, template, params); err != nil {
		return err
	}
	return nil
}

// LoadTemplateFileToData
// is only load once.
func (c *Core) LoadTemplateFileToData(node *ConfigNode) error {
	if node.Data != nil {
		return nil
	}
	path := node.Path + node.Driver + ".toml"
	if gres.Contains(path) {
		file := gres.Get(path)
		if node.Data == nil {
			node.Data = gmap.NewStrStrMap() //make(map[string]string)
		}
		// get content
		if i, err := gjson.LoadContent(file.Content()); err == nil {
			for k, v := range i.ToMap() {
				node.Data.Set(k, gconv.String(v))
			}
		} else {
			glog.Errorf("load sms template file '%s' failed: %v", node.Driver, err)
		}
	} else if path != "" {
		if !gfile.Exists(path) {
			glog.Errorf("load sms template file '%s' failed.", path)
		}
		// node.data = make(map[string]string)
		if node.Data == nil {
			node.Data = gmap.NewStrStrMap() //make(map[string]string)
		}
		if j, err := gjson.LoadContent(gfile.GetBytes(path)); err == nil {
			for k, v := range j.ToMap() {
				node.Data.Set(k, gconv.String(v))
			}
		} else {
			glog.Errorf("load sms template file '%s' failed: %v", node.Driver, err)
		}
	}

	// Hot reload >>> How to do ???
	return nil
}

// init initialize the driver template
func (c *Core) init() {

}
