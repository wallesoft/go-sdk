package sms

import (
	"errors"
	"fmt"

	"github.com/gogf/gf/container/gmap"
	"github.com/gogf/gf/container/gtype"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/grand"
	"github.com/gogf/gf/util/gutil"
)

// SMS defines the interfaces for SMS operations.
type SMS interface {
	Send(mobile string, template string, params map[string]string) error
	SetLogger(logger *glog.Logger)
	GetLogger() *glog.Logger
	// ParseTemplate(template string, args ...interface{}) (error)
	// Excute() error
}

// Core is the base struct for sms management
type Core struct {
	SMS    SMS          // SMS interface object.
	group  string       // Configuration group name.
	debug  *gtype.Bool  // Enable debug mode for the sms.
	logger *glog.Logger //Logger.
	// data   map[string]map[string]string // template data
}

// Driver is the interface for sms drivers
type Driver interface {
	New(core *Core, node *ConfigNode) (SMS, error)
	Send(mobile string, template string, params map[string]string) error
}

var (
	// instance is the management map for instances.
	instances = gmap.NewStrAnyMap(true)
	// driverMap manages all custom registered driver.
	driverMap = map[string]Driver{
		"diyi":   &DriverDiyi{},
		"aliyun": &DriverAliyun{},
	}
)

func init() {
	//config := g.Cfg()
}

// New creates and returns a SMS object with global configuration
// The parameter <name> specifiels the configuration group name
func New(name ...string) (sms SMS, err error) {
	group := configs.group
	if len(name) > 0 && name[0] != "" {
		group = name[0]
	}
	configs.RLock()
	config := g.Cfg()
	if len(configs.config) < 1 {
		configs.RUnlock()
		m := config.GetMap("sms")
		if m == nil {
			panic(`sms init failed: "sms" node not found , is config file or configuration missing?`)
		}
		for group, groupConfig := range m {
			cg := ConfigGroup{}
			switch value := groupConfig.(type) {
			case []interface{}:
				for _, v := range value {
					if node := parseSMSConfigNode(v); node != nil {
						cg = append(cg, *node)
					}
				}

			case map[string]interface{}:
				if node := parseSMSConfigNode(value); node != nil {
					cg = append(cg, *node)
				}
			}
			if len(cg) > 0 {
				SetConfigGroup(group, cg)
			}
		}
	} else {
		defer configs.RUnlock()
	}

	if _, ok := configs.config[group]; ok {
		if node, err := getConfigNodeByGroup(group); err == nil {
			c := &Core{
				group:  group,
				debug:  gtype.NewBool(),
				logger: glog.New(),
			}
			if v, ok := driverMap[node.Driver]; ok {
				// c.SMS, err = v.New(c, node)
				// if err != nil {
				// 	return nil, err
				// }
				// return c.SMS, nil
				if c.SMS, err = v.New(c, node); err == nil {
					m := config.GetMap("sms.logger")
					if m == nil {
						m = config.GetMap("logger")
					}
					if m != nil {
						if err := c.SMS.GetLogger().SetConfigWithMap(m); err != nil {
							panic(err)
						}
					}
					return c.SMS, nil
				} else {
					panic(err)
				}
			} else {
				return nil, errors.New(fmt.Sprintf(`unsupported sms type "s%"`, group))
			}
		} else {
			return nil, err
		}
	} else {
		return nil, errors.New(fmt.Sprintf(`sms configuration node "s%" is not found`, group))
	}
}

// Register registers custom sms driver to sms
func Register(name string, driver Driver) error {
	driverMap[name] = driver
	return nil
}

// Instance get a sms instance
func Instance(name ...string) (sms SMS, err error) {
	group := configs.group
	if len(name) > 0 && name[0] != "" {
		group = name[0]
	}
	v := instances.GetOrSetFuncLock(group, func() interface{} {
		sms, err = New(group)
		return sms
	})
	if v != nil {
		return v.(SMS), nil
	}
	return
}

// getConfigNodeByGroup return a configuration node of given group.
func getConfigNodeByGroup(group string) (*ConfigNode, error) {
	if list, ok := configs.config[group]; ok {
		nodeList := make(ConfigGroup, 0)
		for i := 0; i < len(list); i++ {
			nodeList = append(nodeList, list[i])
		}
		if len(nodeList) < 1 {
			return nil, errors.New("at least one node configuration's need to make sense")
		}
		return getConfigNodeByWeight(nodeList), nil
	} else {
		return nil, errors.New(fmt.Sprintf("empty sms configuration for item name 's%'", group))
	}
}

// getConfigNodeByEqually return config node equally if have more than one configuration by same group name.
func getConfigNodeByWeight(cg ConfigGroup) *ConfigNode {
	if len(cg) < 2 {
		return &cg[0]
	}
	var total int
	for i := 0; i < len(cg); i++ {
		total += cg[i].Weight * 100
	}
	// if total is 0 means all of the nodes have no weight attribute configured.
	if total == 0 {
		for i := 0; i < len(cg); i++ {
			cg[i].Weight = 1
			total += cg[i].Weight * 100
		}
	}

	r := grand.N(0, total)
	min := 0
	max := 0
	for i := 0; i < len(cg); i++ {
		max = min + cg[i].Weight*100
		if r >= min && r <= max {
			return &cg[i]
		} else {
			min = max
		}
	}
	return nil
}

// func Send(mobile string,tmp string，data ...interface{}) {

// }
func parseSMSConfigNode(value interface{}) *ConfigNode {
	nodeMap, ok := value.(map[string]interface{})
	if !ok {
		return nil
	}
	node := &ConfigNode{}
	err := gconv.Struct(nodeMap, node)

	if err != nil {
		panic(err)
	}
	if _, v := gutil.MapPossibleItemByKey(nodeMap, "configNode"); v != nil {
		node.Config = gmap.NewStrAnyMapFrom(gconv.Map(v))
	}
	return node
}
